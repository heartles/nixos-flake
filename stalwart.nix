{ config, pkgs, lib, ... }:

let
  user = "stalwart-mail";
  group = user;
  database = user;
  domain = "mail.heartles.xyz";
in {
  users.users."${user}" = {
    isSystemUser = true;
    group = "${group}";
  };
  users.groups."${group}" = { members = [ user ]; };

  security.acme = {
    acceptTerms = true;
    defaults.email = "admin+acme@heartles.xyz";
    certs."${domain}" = {
      inherit domain group;
      dnsProvider = "namecheap";
      credentialsFile = "/etc/nixos-secrets/namecheap-acme";
    };
  };

  # services.postgresql = {
  #   enable = true;
  #   ensureDatabases = [ "${database}" ];
  #   ensureUsers = [{
  #     name = "${user}";
  #     ensureDBOwnership = true;
  #   }];
  # };

  services.stalwart-mail = {
    enable = true;
    package = let version = "0.7.0";
    in pkgs.stalwart-mail.overrideAttrs (final: prev: {
      inherit version;
      src = pkgs.fetchFromGitHub {
        owner = "stalwartlabs";
        repo = "mail-server";
        rev = "v${version}";
        hash = "sha256-Ah53htK38Bm2yGN44IiC6iRWgxkMVRtrNvXXvPh+SJc=";
        fetchSubmodules = true;
      };
    });
    settings = {
      server.hostname = domain;
      lookup.default = { hostname = domain; };

      server.listener."smtp" = {
        bind = [ "[::]:25" ];
        protocol = "smtp";
      };
    };
  };
}
