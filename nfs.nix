{
  fileSystems."/net/video" = {
    device = "supplydepot:/video";
    fsType = "nfs";
    options = [ "nfsvers=4.1" "x-systemd.automount" "noauto" ];
  };
}
