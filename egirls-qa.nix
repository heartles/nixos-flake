{ pkgs, ... }: {
  security.acme = {
    acceptTerms = true;
    defaults.email = "admin+acme@heartles.xyz";
    certs."ANY.dev.egirls.gay" = {
      domain = "*.dev.egirls.gay";
      dnsProvider = "namecheap";
      credentialsFile = "/etc/nixos-secrets/namecheap-acme";
      group = "nginx";
    };
  };
  services.nginx = {
    enable = true;
    recommendedProxySettings = true;
    package = pkgs.openresty;
    proxyCachePath."media_cache" = {
      enable = true;
      maxSize = "1g";
      inactive = "10m";
      keysZoneName = "media_cache";
    };
    virtualHosts = let
      mkHost = upstream: {
        listen = [
          {
            port = 443;
            addr = "0.0.0.0";
            ssl = true;
          }
          {
            port = 80;
            addr = "0.0.0.0";
          }
        ];
        useACMEHost = "ANY.dev.egirls.gay";
        forceSSL = true;
        locations."/" = {
          proxyWebsockets = true;
          proxyPass = upstream;
        };
      };
    in {
      "masto1.dev.egirls.gay" = {
        listen = [
          {
            port = 443;
            addr = "0.0.0.0";
            ssl = true;
          }
          {
            port = 80;
            addr = "0.0.0.0";
          }
        ];
        useACMEHost = "ANY.dev.egirls.gay";
        forceSSL = true;
        locations."/" = {
          proxyWebsockets = true;
          proxyPass = "http://172.16.128.1";
        };
      };
      "eg.dev.egirls.gay" = mkHost "http://127.0.0.1:3000";
      "goto.dev.egirls.gay" = mkHost "http://127.0.0.1:8080";
"media.dev.egirls.gay" = {
      listen = [
        {
          port = 443;
          addr = "0.0.0.0";
          ssl = true;
        }
        {
          port = 80;
          addr = "0.0.0.0";
        }
      ];
      useACMEHost = "ANY.dev.egirls.gay";
      forceSSL = true;

      extraConfig = ''

        proxy_cache media_cache;
        proxy_cache_valid 200 10m;
        proxy_cache_lock on;

        add_header X-Cache $upstream_cache_status;
        proxy_ignore_headers X-Accel-Expires Expires Cache-Control;
        proxy_hide_header X-Amz-ID-2;
        proxy_hide_header X-Amz-Request-ID;
        proxy_hide_header X-Wasabi-CM-Reference-ID;

        proxy_hide_header      Set-Cookie;
        proxy_ignore_headers   Set-Cookie;
      '';

      locations."/".extraConfig = "return 404;";

      locations."/misskey/" = {
        #recommendedProxySettings = true;
        extraConfig = ''
          proxy_http_version 1.1;

          include /etc/nixos-secrets/s3-access-nginx.conf;

          set $s3_bucket 'egirls-gay-misskey';
          set $path_full '/$s3_bucket$request_uri';

          set_by_lua $now "return ngx.http_time(ngx.time())";
          set $signature_string "GET\n\n\n\nx-amz-date:''${now}\n$path_full";
          set_hmac_sha1 $s3_signature $s3_secret $signature_string;
          set_encode_base64 $s3_signature_b64 $s3_signature;

          proxy_set_header x-amz-date $now;
          proxy_set_header Authorization "AWS $s3_access:$s3_signature_b64";

          proxy_ssl_session_reuse on;
          rewrite .* $path_full break;
          proxy_pass https://s3.us-west-1.wasabisys.com;
        '';
      };
    };
  };

  networking.nat = {
    enable = true;
    internalInterfaces = [ "ve-+" ];
    externalInterface = "wlp0s20f3";
    enableIPv6 = true;
  };

  networking.hosts = {
    "127.0.0.1" =
      [ "masto1.dev.egirls.gay" "eg.dev.egirls.gay" "goto.dev.egirls.gay" ];
    #"5.78.100.40" = [ "jellyfin.home.heartles.xyz" ];
  };
  networking.firewall.allowedTCPPortRanges = [{
    from = 55000;
    to = 55100;
  }];
}
