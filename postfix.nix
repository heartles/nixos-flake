{ pkgs, ... }:

{
  services.postfix = {
    enable = true;
    enableSubmission = true;

    extraConfig = ''
      inet_interfaces = 127.0.0.1
    '';
  };
}
