# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, unstable, hostName, ... }:

with lib;
with builtins;

let
  # unstableTarball = fetchTarball
  #   "https://github.com/NixOS/nixpkgs-channels/archive/nixos-unstable.tar.gz";
  #unstable = import unstableTarball { config = config.nixpkgs.config; };
in {
  imports = [ # Include the results of the hardware scan.
    ./hardware-configuration.nix
    #./egirls-qa.nix
    ./postfix.nix
    ./nebula.nix
    ./gpu.nix
    #./stalwart.nix
    #./vpn.nix
    #/home/jaina/src/nix-deployments/nordvpn/containers.nix
    #/home/jaina/src/nix-deployments/refactor/nixos-containers.nix
    #/home/jaina/src/nix-deployments/arion/arion.nix
  ];

  # networking.bridges.vpnbr = { interfaces = [ "vpnif0" ]; };
  # networking.interfaces.vpnif0 = {
  #   virtual = true;
  # };
  fileSystems = {
    "/".options = [ "compress=zstd" ];
    "/home".options = [ "compress=zstd" ];
    "/nix".options = [ "compress=zstd" "noatime" ];
  };

  nix.settings = {
    experimental-features = [ "nix-command" "flakes" ];
    auto-optimise-store = true;
  };

  nix.gc = {
    automatic = true;
    dates = "weekly";
    options = "--delete-older-than 30d";
  };

  programs.gamemode.enable = true;

  programs.nix-ld = {
    enable = true;
    libraries = with pkgs; [
      glib
      nss
      nspr
      atk
      cups
      dbus
      libdrm

      # hypothesized
      gtk3
      pango
      cairo
      xorg.libX11
      xorg.libXcomposite
      xorg.libXdamage
      xorg.libXext
      xorg.libXfixes
      xorg.libXrandr
      mesa
      expat
      xorg.libxcb
      libxkbcommon
      alsa-lib
      at-spi2-atk
    ]; # For foundryvtt
  };

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  services.thermald.enable = true;

  programs.steam.enable = true;

  services.xserver.videoDrivers = [ "modesetting" ];

  specialisation.nouveau.configuration = { jaina.gpuDriver = "nouveau"; };
  specialisation.intel.configuration = { jaina.gpuDriver = "intel"; };

  hardware.opengl = {
    enable = true;
    #package = pkgs.mesa.override { galliumDrivers = [ "zink" "r600" "swrast" "nouveau" "i915" "iris" "auto" ]; };
    driSupport = true;
    driSupport32Bit = true;
    extraPackages = with pkgs; [
      libvdpau-va-gl
      vaapiVdpau
      intel-ocl
      intel-media-driver
      intel-gpu-tools
    ];
  };

  environment.sessionVariables = { LIBVA_DRIVER_NAME = "iHD"; };

  programs.extra-container.enable = true;
  # Pick only one of the below networking options.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  networking.networkmanager.enable =
    true; # Easiest to use and most distros use this by default.

  hardware.cpu.intel.updateMicrocode = true;

  # Set your time zone.
  #time.timeZone = "America/Los_Angeles";
  # services.geoclue2 = {
  #   enable = true;
  #   submitData = true;
  #   enableModemGPS = false;
  #   enableNmea = false;
  #   enable3G = false;
  #   enableCDMA = false;
  # };
  # services.localtimed.enable = true;
  services.tzupdate.enable = true;

  # specialisation = {
  #   home.configuration = { time.timeZone = "America/Los_Angeles"; };
  #   cest.configuration = { time.timeZone = "Europe/Amsterdam"; };
  # };

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  # i18n.defaultLocale = "en_US.UTF-8";
  # console = {
  #   font = "Lat2-Terminus16";
  #   keyMap = "us";
  #   useXkbConfig = true; # use xkbOptions in tty.
  # };

  # Enable the X11 windowing system.
  # services.xserver.config = ''
  #   Section "Device"
  #     #Identifier "Intel Corporation Alder Lake-P Integrated Graphics"
  #     Identifier "Intel Graphics"
  #     #Identifier "Intel(R) Graphics"
  #     BusID "PCI:0:2:0"
  #     Driver "Intel"
  #     Option "DRI" "3"
  #   EndSection
  # '';
  # services.xserver.enable = true;
  # services.xserver.displayManager.startx.enable = true;

  # Enable the GNOME Desktop Environment.
  #services.xserver.displayManager.gdm.enable = true;
  #services.xserver.desktopManager.gnome.enable = true;

  #services.xserver.displayManager.sddm.enable = true;
  #services.xserver.desktopManager.plasma5.enable = true;

  # Configure keymap in X11
  # services.xserver.layout = "us";
  # services.xserver.xkbOptions = {
  #   "eurosign:e";
  #   "caps:escape" # map caps to escape.
  # };

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  # sound.enable = true;
  # hardware.pulseaudio.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  virtualisation.libvirtd.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.jaina = {
    isNormalUser = true;
    uid = 1000;
    extraGroups = [ "wheel" "networkmanager" "libvirtd" "adbusers" "cdrom" ];
    packages = with pkgs; [ fish ];
    shell = pkgs.fish;
  };
  programs.fish.enable = true;

  programs.adb.enable = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    neovim
    wget
    less
    killall
    python3
    htop
    openssh
    git

    interception-tools

    cifs-utils
    ntfs3g

    tlp

    bridge-utils
    vulkan-tools

    lm_sensors

    gamescope
    intel-gpu-tools

    sshfs

    docker
  ];

  programs.sway.enable = true;
  #programs.sway.package = pkgs.sway;
  # programs.sway.package = pkgs.sway.override {
  #   sway-unwrapped = pkgs.sway-unwrapped.override {
  #     wlroots_0_16 = pkgs.wlroots_0_16.overrideAttrs {
  #       patches = [
  #         (pkgs.fetchpatch {
  #           url =
  #             "https://gitlab.freedesktop.org/wlroots/wlroots/uploads/b4f932e370ed03d88f202191eaf60965/DisplayLink.patch";
  #           sha256 = lib.fakeHash;
  #         })
  #       ];
  #     };
  #   };
  # };

  xdg.portal = {
    enable = true;
    #wlr.enable = false;
    extraPortals =
      [ pkgs.xdg-desktop-portal-gtk pkgs.xdg-desktop-portal-hyprland ];
  };

  services.interception-tools = {
    enable = true;
    plugins = [ pkgs.interception-tools-plugins.caps2esc ];
    udevmonConfig = ''
      - JOB: "${pkgs.interception-tools}/bin/intercept -g $DEVNODE | ${pkgs.interception-tools-plugins.caps2esc}/bin/caps2esc | ${pkgs.interception-tools}/bin/uinput -d $DEVNODE"
        DEVICE:
          EVENTS:
            EV_KEY: [KEY_CAPSLOCK, KEY_ESC]
    '';
  };

  security.pam.services.swaylock = {
    name = "swaylock";
    text = "auth include login";
  };

  hardware.bluetooth.enable = true;
  #services.hardware.bolt.enable = true;
  services.upower.enable = true;
  services.logind.extraConfig = ''
    HandlePowerKey=suspend
  '';
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    audio.enable = true;
    pulse.enable = true;
    jack.enable = true;
    alsa.enable = true;

    wireplumber.enable = true;
  };

  programs.hyprland.enable = true;
  programs.dconf.enable = true;

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [
    22000 # syncthing
  ];
  networking.firewall.allowedUDPPorts = [
    # syncthing
    22000
    21027
  ];

  # networking.nat = {
  #   enable = true;
  #   internalInterfaces = [ "ve-vpn" ];
  #   externalInterface = "wlp0s20f3";
  #   # Lazy IPv6 connectivity for the container
  #   enableIPv6 = true;
  # };

  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.11"; # Did you read the comment?

}

