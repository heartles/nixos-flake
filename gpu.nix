{ config, lib, ... }:

let cfg = config.jaina;
in {
  options = {
    jaina.gpuDriver = lib.mkOption {
      type = lib.types.str;
      default = "nvidia";
    };
  };
  config = lib.mkMerge [
    (lib.mkIf (cfg.gpuDriver == "nvidia") {
      services.xserver.videoDrivers = [ "nvidia" ];
      hardware.nvidia.modesetting.enable = true;
      hardware.nvidia.open = true;
      hardware.nvidia.package =
        config.boot.kernelPackages.nvidiaPackages.stable;
      boot.blacklistedKernelModules = [ "nouveau" ];
    })
    (lib.mkIf (cfg.gpuDriver == "nouveau") {
      services.xserver.videoDrivers = [ "nouveau" ];
      boot.blacklistedKernelModules = [ "nvidia" "nvidia-drm" ];
    })
    (lib.mkIf (cfg.gpuDriver == "intel") {
      boot.blacklistedKernelModules = [ "nouveau" "nvidia" "nvidia-drm" ];
    })
  ];
}
