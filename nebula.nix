{ lib, pkgs, config, ... }:

{
  environment.systemPackages = with pkgs; [ nebula ];
  services.nebula.networks.home = {
    enable = true;
    cert = "/var/lib/nebula/${config.networking.hostName}.crt";
    key = "/var/lib/nebula/${config.networking.hostName}.key";
    ca = "/var/lib/nebula/ca.crt";

    lighthouses = [ "172.16.0.1" ];
    staticHostMap."172.16.0.1" = [ "5.78.100.40:4242" ];

    firewall.inbound = [{
      host = "any";
      port = "any";
      proto = "any";
    }];
    firewall.outbound = [{
      host = "any";
      port = "any";
      proto = "any";
    }];
  };

  networking.hosts."172.16.0.2" =
    [ "torrent.home" "sonarr.home" "radarr.home" "prowlarr.home" ];
}
