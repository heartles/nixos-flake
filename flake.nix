{
  inputs = {
    #nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.05";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05";
    stable.url = "github:NixOS/nixpkgs/nixos-24.05";
    unstable.url = "github:NixOS/nixpkgs/nixos-unstable";

    home-manager = {
      url = "github:nix-community/home-manager/release-24.05";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };
  outputs = { self, nixpkgs, stable, unstable, home-manager, ... }@attrs: {
    nixosConfigurations = let
      mkNixosConfig = hostName:
        nixpkgs.lib.nixosSystem rec {
          pkgs = let
            nixpkgsConfig = {
              inherit system;
              config.allowUnfree = true;
            };
          in import nixpkgs (nixpkgsConfig // {
            overlays = [
              (new: prev: {
                unstable = import unstable nixpkgsConfig;
                stable = import stable nixpkgsConfig;
              })
            ];
          });
          system = "x86_64-linux";
          modules = [
            ./configuration.nix
            # This fixes nixpkgs (for e.g. "nix shell") to match the system nixpkgs
            ({ config, pkgs, options, ... }: {
              nix.registry.nixpkgs.flake = nixpkgs;
              networking.hostName = hostName; # Define your hostname.
            })

            home-manager.nixosModules.home-manager
            {
              home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;
              home-manager.users.jaina = import ./home-manager/home.nix;
              home-manager.backupFileExtension = "hmbackup";
            }
          ];
        };
    in {
      harrowhark = mkNixosConfig "harrowhark";
      murray = mkNixosConfig "murray";
    };
  };
}
