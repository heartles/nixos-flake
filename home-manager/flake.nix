{
  description = "home manager flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/release-24.05";
    stable.url = "github:nixos/nixpkgs/release-23.05";
    unstable.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    nixgl.url = "github:guibou/nixGL";
    home-manager = {
      url = "github:nix-community/home-manager/release-24.05";
      inputs.nixpkgs.follows = "nixpkgs";
      # url = "github:nix-community/home-manager/master";
      #inputs.nixpkgs.follows = "unstable";
    };
  };

  outputs = { nixpkgs, stable, unstable, home-manager, nixgl, ... }: {
    defaultPackage.x86_64-linux = home-manager.defaultPackage.x86_64-linux;

    homeConfigurations = let system = "x86_64-linux";
    in rec {
      "jaina" = home-manager.lib.homeManagerConfiguration {
        pkgs = let
          nixpkgsConfig = {
            inherit system;
            config.allowUnfree = true;
          };
        in import nixpkgs (nixpkgsConfig // {
          overlays = [
            (new: prev: {
              unstable = import unstable nixpkgsConfig;
              stable = import stable nixpkgsConfig;
            })
            nixgl.overlay
          ];
        });

        #inputs.cat = import ./catppuccin/mocha.nix;

        modules = [ ./home.nix ];
      };
    };
  };
}
