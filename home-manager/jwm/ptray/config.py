import os
import json

CONFIG_FILE = os.path.join(os.getenv('XDG_CONFIG_HOME'), 'ptray', 'config.json')

DESKTOP = os.getenv('XDG_CURRENT_DESKTOP')

if DESKTOP == 'i3' or DESKTOP == 'sway':
    STATE_DIR = '{}.{}'.format(os.getenv('I3SOCK'), 'ptray')
else:
    raise ValueError('Unsupported value of XDG_CURRENT_DESKTOP: {}'.format(DESKTOP))

def read_config(path=CONFIG_FILE):
    with open(path, 'r') as f:
        config = json.load(f)
    return config
