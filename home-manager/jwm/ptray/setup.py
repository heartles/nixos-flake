#!/usr/bin/env python

from distutils.core import setup

setup(
    name='ptray',
    version='0.1',
    scripts=[ 'ptray', 'ptrayctl' ],
    packages=['', 'i3sway'],
)
