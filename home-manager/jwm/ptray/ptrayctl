#!/usr/bin/env python

import os
import argparse
import inotify.adapters
from i3sway.commands import Client
from config import read_config
from state import open_file, format_path

parser = argparse.ArgumentParser(
    prog='ptrayctl',
    description='manages user pseudo-tray',
)

parser.add_argument('command', choices=['show', 'hide', 'watch', 'toggle'])
parser.add_argument('name', nargs='?')
parser.add_argument('-a', '--all', action='store_true', help='Include windows that are not floating')

args = parser.parse_args()
config = read_config()

def show_window(name):
    with Client() as client:
        is_open = False
        with open_file(name, 'window_ids') as f:
            for line in f:
                is_open = True
                client.show_window(int(line))
        if not is_open:
            with open_file(name, 'exec') as f:
                client.run(f.read())

def hide_window(name):
    if not args.all:
        with open_file(name, 'tiled_ids') as f:
            ignore_ids = f.read().split('\n')
    else:
        ignore_ids = []

    with Client() as client:
        with open_file(name, 'window_ids') as f:
            for line in f:
                if line not in ignore_ids:
                    client.hide_window(int(line))

def hide_all():
    for name in config['items']:
        hide_window(name)

def toggle_window(name):
    with open_file(name, 'status') as f:
        status = f.read()
    if status == 'fg':
        hide_window(name)
    else:
        show_window(name)

def watch_window(name):
    i = inotify.adapters.Inotify()
    i.add_watch(format_path(name, ''))
    def read_status():
        with open_file(name, 'status') as f:
            return f.read()

    status = read_status()
    print(status, flush=True)

    for event in i.event_gen(yield_nones=False):
        (_, type_names, _, filename) = event
        if 'IN_CLOSE_WRITE' in type_names and filename == 'status':
            new_status = read_status()
            if status != new_status:
                print(new_status, flush=True)
                status = new_status

if args.command == 'show':
    if args.name is None:
        raise ValueError('name must be specified for show commands')

    show_window(args.name)
elif args.command == 'hide':
    if args.name is None:
        hide_all()
    else:
        hide_window(args.name)
elif args.command == 'toggle':
    if args.name is None:
        raise ValueError('name must be specified for toggle commands')

    toggle_window(args.name)
elif args.command == 'watch':
    if args.name is None:
        raise ValueError('name must be specified for watch commands')

    watch_window(args.name)

