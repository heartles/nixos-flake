import os
from config import STATE_DIR

def format_path(name, property):
    return '{}/{}/{}'.format(STATE_DIR, name, property)


def write_file(dir, basename, content):
    os.makedirs('{}/{}'.format(STATE_DIR, dir), exist_ok=True)
    path = format_path(dir, basename)
    with open(path, 'w') as f:
        f.write(content)

def open_file(dir, basename):
    path = format_path(dir, basename)
    return open(path, 'r')

def add_window(tracking: dict, name, window_id: str):
    new_dict = tracking.copy()
    new_dict[name] = new_dict.get(name, set()) | {window_id}
    return new_dict

def remove_window(tracking: dict, name, window_id: str):
    new_dict = tracking.copy()
    new_dict[name] = new_dict.get(name, set()) - {window_id}
    return new_dict

def read_config(path):
    with open(path, 'r') as f:
        config = json.load(f)
    return config

class StateWriter:
    tracked_ids = None
    visible_ids = None
    config = None

    def __init__(self, config):
        self.config = config
        self.tracked_ids = {name: set() for name in config['items'].keys()}
        self.visible_ids = {name: set() for name in config['items'].keys()}
        self.tiled_ids = {name: set() for name in config['items'].keys()}
        for name, defn in config['items'].items():
            write_file(name, 'exec', defn['exec'])
            write_file(name, 'status', '')
            write_file(name, 'window_ids', '')

    def update_window_state(self, name):
        if len(self.visible_ids[name]) != 0:
            write_file(name, 'status', 'fg')
        elif len(self.tracked_ids[name]) != 0:
            write_file(name, 'status', 'bg')
        else:
            write_file(name, 'status', '')

        write_file(name, 'window_ids', '\n'.join(self.tracked_ids[name]))
        write_file(name, 'tiled_ids', '\n'.join(self.tiled_ids[name]))

    def on_visible(self, item_name, window_id, tiled=False):
        self.visible_ids[item_name] = self.visible_ids[item_name] | {window_id}
        self.update_window_state(item_name)

    def on_invisible(self, item_name, window_id):
        self.visible_ids[item_name] = self.visible_ids[item_name] - {window_id}
        self.update_window_state(item_name)

    def on_create(self, item_name, window_id):
        self.tracked_ids[item_name] = self.tracked_ids[item_name] | {window_id}
        self.update_window_state(item_name)

    def on_destroy(self, item_name, window_id):
        self.tracked_ids[item_name] = self.tracked_ids[item_name] - {window_id}
        self.visible_ids[item_name] = self.visible_ids[item_name] - {window_id}
        self.tiled_ids[item_name] = self.tiled_ids[item_name] - {window_id}
        self.update_window_state(item_name)

    def on_tile(self, item_name, window_id):
        self.tiled_ids[item_name] = self.tiled_ids[item_name] | {window_id}
        self.update_window_state(item_name)

    def on_float(self, item_name, window_id):
        self.tiled_ids[item_name] = self.tiled_ids[item_name] - {window_id}
        self.update_window_state(item_name)


