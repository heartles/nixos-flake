from i3sway.protocol import Connection, ConnType

class Client:
    conn = None
    def __init__(self):
        self.conn = Connection()

    def __enter__(self):
        return self

    def __exit__(self, *details):
        self.conn.close()

    def cmd_on_window(self, id, cmd):
        if self.conn.type == ConnType.SWAY:
            criteria = '[con_id={}]'.format(id)
        else:
            criteria = '[id={}]'.format(id)

        return self.conn.exec('{} {}'.format(criteria, cmd))

    def show_window(self, id):
        self.cmd_on_window(id, "focus")

    def hide_window(self, id):
        self.cmd_on_window(id, "move to scratchpad")

    def run(self, command):
        self.conn.exec("exec {}".format(command))

