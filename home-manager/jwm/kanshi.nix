{ config, lib, pkgs, ... }:

with lib;
with builtins;

let cfg = config.jainawm.kanshi;
in {
  options.jainawm.kanshi = { enable = mkEnableOption "Enable kanshi config"; };

  config = mkIf cfg.enable {
    wayland.windowManager.sway.config.startup = [{
      command = "${pkgs.kanshi}/bin/kanshictl reload";
      always = true;
    }];
    home.packages = [ pkgs.kanshi ];
    services.kanshi = {
      enable = true;
      profiles = let
        laptop = "eDP-1";
        asus1440 = "Ancor Communications Inc ASUS MG278 0x00001EC0";
        hp1080 = "HP Inc. HP 24mh 3CM2023MLQ   ";
        #asus1440 = "DP-3";
        #hp1080 = "DP-2";
      in {
        undocked = {
          outputs = [{
            criteria = laptop;
            mode = "1920x1200";
          }];
        };

        undocked-game = {
          outputs = [{
            criteria = laptop;
            mode = "1920x1080";
            transform = "normal";
            status = "enable";
          }];
        };

        docked = let
          # asusCoords = {
          #   x = 0;
          #   y = 0;
          # };
          asusCoords = {
            x = 1080;
            y = 100;
          };
          mkPosition = x: y:
            "${toString (asusCoords.x + x)},${toString (asusCoords.y + y)}";
        in {
          outputs = [
            {
              criteria = laptop;
              mode = "1920x1200";
              #position = "3640,1080";
              position = mkPosition 2560 980;
              transform = "normal";
              status = "enable";
            }
            {
              criteria = asus1440;
              mode = "2560x1440"; # TODO: figure out 144Hz
              #position = "1080,100";
              position = mkPosition 0 0;
              transform = "normal";
              status = "enable";
            }
            {
              criteria = hp1080;
              mode = "1920x1080";
              #position = "0,0";
              #position = "-1080,100";
              position = mkPosition (-1080) 100;
              transform = "90";
              status = "enable";
            }
          ];
        };
      };
    };
  };
}
