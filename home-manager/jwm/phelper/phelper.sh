#!/bin/sh

profile_dir="$XDG_CONFIG_HOME/phelper/profiles"
active_profile_file="$XDG_STATE_HOME/phelper/active-profile"

get_option () {
    what="$1"
    who="$2"
    if [ -z "$who" ]; then
        who="$(phelper who)"
    fi

    path="$profile_dir/$who"

    case "$what" in
        "display-name") cat "$path/display-name" ;;
        "wallpaper") echo "$path/wallpaper" ;;
        "nicknames") cat "$path/nicknames" ;;
        #"theme_color") echo "$theme_color" ;;
        *)
            echo "Unknown var"
            exit 1
            ;;
    esac
}

do_switch () {
    who="$1"
    file="$XDG_STATE_HOME/phelper/base-generation/specialisation/$who/activate"
    if [ -e "$file" ]; then
        "$file"
    else
        echo "Profile $who not setup"
        exit 1
    fi
}

arg="$1"
shift 1
case "$arg" in
    "list") ls -1 "$profile_dir" ;;
    "switch") do_switch "$@" ;;
    "who") cat "$active_profile_file" ;;
    "get") get_option "$@" ;;
esac

