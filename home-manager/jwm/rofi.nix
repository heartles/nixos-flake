{ config, lib, pkgs, ... }:

with lib;
let
  cfg = config.jainawm;
  cat = cfg.catppuccin;
in {
  options.jainawm.rofi = {
    enable = mkOption {
      type = types.bool;
      default = true;
    };

    font = mkOption { type = lib.hm.types.fontType; };

    accents = {
      primary = mkOption { type = types.str; };
      secondary = mkOption { type = types.str; };
    };
  };

  config = {
    programs.rofi = let font = cfg.rofi.font;
    in {
      enable = true;
      package = pkgs.rofi-wayland;
      plugins = with pkgs; [ rofi-emoji ];
      font = "${font.name} ${builtins.toString font.size}";
      location = "top-left";

      cycle = true;
      terminal = cfg.terminal.binPath;
      xoffset = 10;
      yoffset = 10;

      theme = let
        font = cfg.fonts.wmOverlay;
        inherit (config.lib.formats.rasi) mkLiteral;
      in {
        "*" = {
          transparency = "real";
          background-color = mkLiteral "#00000000";
          text-color = mkLiteral cat.text;
        };

        configuration = {
          show-icons = true;
          # drun = {
          #   display-name = "";
          # };
        };

        window = {
          background-color = mkLiteral "${cat.base}cc";
          border = mkLiteral "3px";
          border-color = mkLiteral cfg.rofi.accents.primary;
          border-radius = mkLiteral "10px";
          x-offset = mkLiteral "10px";
          y-offset = mkLiteral "10px";
          width = mkLiteral "500px";
          location = mkLiteral "northwest";
        };

        mainbox = { padding = mkLiteral "1em"; };

        inputbar = {
          margin = mkLiteral "0px 5px";
          children = [ (mkLiteral "prompt") (mkLiteral "entry") ];
        };

        prompt = { vertical-align = mkLiteral "0.5"; };

        entry = {
          border-radius = mkLiteral "100%";
          padding = mkLiteral "5px 15px";
          margin = mkLiteral "0px 0px 0px 10px";
          background-color = mkLiteral "${cat.overlay0}cc";
          vertical-align = mkLiteral "0.5";
        };

        message = {
          horizontal-align = mkLiteral "1.0";
          text-color = mkLiteral cat.subtext0;
          margin = mkLiteral "0px 5px";
        };

        textbox = {
          horizontal-align = mkLiteral "inherit";
          text-color = mkLiteral "inherit";
        };

        listview = {
          margin = mkLiteral "15px 0px 0px 0px";
          lines = 10;
          fixed-height = false;
          dynamic = true;
          cycle = false;
        };

        element = {
          border-radius = mkLiteral "4px";
          padding = mkLiteral "5px";
        };

        "element.active" = {
          #background-color = mkLiteral "${cat.overlay0}cc";
          border = mkLiteral "2px";
          border-color = mkLiteral cfg.rofi.accents.secondary;
        };

        "element.selected" = {
          background-color = mkLiteral cfg.rofi.accents.primary;
          text-color = mkLiteral cat.base;
        };

        element-icon = { size = mkLiteral "20px"; };

        element-text = {
          text-color = mkLiteral "inherit";
          vertical-align = mkLiteral "0.5";
          margin = mkLiteral "0px 0px 0px 5px";
        };
      };
    };
  };
}
