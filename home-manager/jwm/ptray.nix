{ lib, pkgs, config, ... }:

with lib;
with builtins;

let cfg = config.jainawm.ptray;
in {
  options.jainawm.ptray = {
    enable = mkEnableOption "Enable ptray support";
    items = mkOption {
      type = types.anything;
      default = { };
    };

    style = mkOption {
      type = types.listOf types.str;
      default = [
        "floating enable"
        "opacity 0.97"
        "resize set 80ppt 80ppt"
        "move position 10ppt -10"
        "sticky enable"
      ];
    };

    finalPackage = mkOption {
      type = types.package;
      readOnly = true;
    };
  };

  config = mkIf cfg.enable {
    wayland.windowManager.sway.config.window.commands = let
      mkRules = let
        style = concatStringsSep ",, " cfg.style;
        mkRulesForClass = class: [
          ({
            command = style;
            criteria = { class = class; };
          })
          ({
            command = style;
            criteria = { app_id = class; };
          })
        ];
      in name: item: map mkRulesForClass item.match_classes;
    in lists.flatten (mapAttrsToList mkRules cfg.items);
    wayland.windowManager.sway.config.startup =
      [{ command = "${cfg.finalPackage}/bin/ptray"; }];

    jainawm.ptray.finalPackage =
      # prob not the right way to do this but i don't really care
      let python = pkgs.python311;
      in python.pkgs.buildPythonPackage {
        name = "jwm-ptray";
        version = "0.1";

        src = ./ptray;

        doCheck = false;

        propagatedBuildInputs = with python.pkgs; [ inotify ];
      };

    xdg.configFile."ptray/config.json".text =
      builtins.toJSON ({ items = cfg.items; });
    home.packages = [ cfg.finalPackage ];
  };
}
