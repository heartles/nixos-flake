{ config, lib, pkgs, ... }:

with lib;
let
  cfg = config.jainawm;
  cat = cfg.catppuccin;
  profileDir = "${config.xdg.stateHome}/phelper";
  profilePath = "${profileDir}/active-profile";

in {
  options.jainawm.phelper = {
    enable = mkOption {
      type = types.bool;
      default = true;
    };

    defaultProfile = mkOption { type = types.str; };
    specialisationProfile = mkOption {
      type = types.nullOr types.str;
      default = null;
    };
    makeSpecializations = mkOption {
      type = types.bool;
      default = false;
    };

    profiles = mkOption {
      type = types.attrsOf (types.submodule {
        options = {
          name = mkOption { type = types.str; };

          displayName = mkOption {
            type = types.nonEmptyStr;
            description = ''
              Display name to use for the profile.
            '';
          };

          nicknames = mkOption {
            type = types.listOf types.str;
            default = [ ];
          };

          accents = let
            colorType = types.enum [
              "Rosewater"
              "Flamingo"
              "Pink"
              "Mauve"
              "Red"
              "Maroon"
              "Peach"
              "Yellow"
              "Green"
              "Teal"
              "Sky"
              "Sapphire"
              "Blue"
              "Lavender"
            ];
          in {
            primary = mkOption {
              type = colorType;
              description = "The primary accent to use.";
            };
            secondary = mkOption {
              type = colorType;
              description = "The secondary accent to use.";
            };
          };

          wallpaper = mkOption { type = types.path; };
        };
      });
    };
  };

  config = let
    useDefault = builtins.isNull cfg.phelper.specialisationProfile;
    activeProfileName = if !useDefault then
      cfg.phelper.specialisationProfile
    else
      cfg.phelper.defaultProfile;
    activeProfile = getAttr activeProfileName cfg.phelper.profiles;
  in mkIf cfg.phelper.enable {
    jainawm = {
      wallpaper = activeProfile.wallpaper;
      accents = activeProfile.accents;
    };

    assertions = [{
      assertion = config.xdg.userDirs.enable;
      message = "xdg.userDirs.enable must be set";
    }];

    home.activation.phelperSetProfile =
      let path = "${config.xdg.stateHome}/phelper/active-profile";
      in lib.hm.dag.entryAfter [ "writeBoundary" ] ''
        $DRY_RUN_CMD mkdir -p "${config.xdg.stateHome}/phelper"
        $DRY_RUN_CMD echo "${activeProfile.name}" > "${path}"
      '';

    # When switching to a "base" (not a profile specialisation) generation,
    # keep track of what the
    home.activation.phelperTrackSpecialization = lib.mkDefault
      (lib.hm.dag.entryAfter [ "writeBoundary" ] ''
        $DRY_RUN_CMD rm -f "${config.xdg.stateHome}/phelper/base-generation"
        $DRY_RUN_CMD ${pkgs.coreutils}/bin/ln -s "$(
          ${pkgs.home-manager}/bin/home-manager generations | \
            ${pkgs.coreutils}/bin/head -n1 | \
            ${pkgs.gawk}/bin/awk '{ print $7; }'
          )" \
          "${config.xdg.stateHome}/phelper/base-generation"
      '');

    specialisation = mkIf useDefault (mapAttrs (n: c: {
      configuration = {
        jainawm.phelper.specialisationProfile = n;
        home.activation.phelperTrackSpecialization =
          lib.hm.dag.entryAnywhere "";
      };
    }) cfg.phelper.profiles);

    xdg.configFile = let
      mkFiles = name: profile: {
        "phelper/profiles/${name}/name".text = profile.name;
        "phelper/profiles/${name}/display-name".text = profile.displayName;
        "phelper/profiles/${name}/nicknames".text =
          lib.concatStringsSep "\n" profile.nicknames;
        "phelper/profiles/${name}/wallpaper".source = profile.wallpaper;
        "phelper/profiles/${name}/wallpaper.${
          builtins.unsafeDiscardStringContext
          (lib.lists.last (lib.strings.splitString "." profile.wallpaper))
        }".source = profile.wallpaper;
      };
    in lists.foldl lib.trivial.mergeAttrs { }
    (mapAttrsToList mkFiles cfg.phelper.profiles);

    home.packages = let
      pkg = pkgs.stdenv.mkDerivation {
        name = "phelper-tools";

        dontUnpack = true;

        installPhase = ''
          mkdir -p "$out/bin"
          cp "${./phelper/phelper.sh}" "$out/bin/phelper"
        '';
      };
    in [ pkg ];
  };
}
