{ config, lib, pkgs, nixpkgs, ... }:

with lib;
let
  cfg = config.jainawm;
  ptray = config.jainawm.ptray.finalPackage;
in {
  options.jainawm.sway = {
    enable = mkEnableOption "Enable the use of swaywm, a tiling window manager";
    useFx = mkEnableOption "Replace sway with swayfx";
  };

  config = let cat = cfg.catppuccin;
  in mkIf cfg.enable {

    home.packages = with pkgs; [ wlr-randr ];
    wayland.windowManager.sway = {
      enable = cfg.sway.enable;

      # package = pkgs.sway.override {
      #   sway-unwrapped = pkgs.sway-unwrapped.override {
      #     wlroots_0_16 = pkgs.wlroots_0_16.overrideAttrs (final: prev: {
      #       pname = prev.pname + "-displaylink";
      #       patches = [
      #         (pkgs.fetchpatch {
      #           url =
      #             "https://gitlab.freedesktop.org/wlroots/wlroots/uploads/b4f932e370ed03d88f202191eaf60965/DisplayLink.patch";
      #           sha256 = "sha256-1HheLsOSnj4OMiA35QCHkWprTNgAeG2tXrGbaQyUrF4=";
      #         })
      #       ];
      #     });
      #   };
      # };

      package = let c = config.wayland.windowManager.sway;
      in mkIf cfg.sway.useFx (pkgs.unstable.sway.override {
        sway-unwrapped = pkgs.unstable.swayfx;
        extraSessionCommands = c.extraSessionCommands;
        extraOptions = c.extraOptions;
        withBaseWrapper = c.wrapperFeatures.base;
        withGtkWrapper = c.wrapperFeatures.gtk;
      });

      systemd.enable = true;
      config = {
        modifier = "Mod4";
        bars = [{ command = "waybar"; }];
        gaps.inner = 20;

        fonts = let font = cfg.fonts.primary;
        in {
          names = [ font.name ];
          # NOTE: add 0.0 to force conversion to floating point rational
          # TODO: Need to cast integer to floating point, what's the right way?
          size = font.size + 0.0;
        };

        colors = {
          focused = {
            border = cat.surface2;
            background = cat.surface2;
            text = cat.text;
            indicator = cat.green;
            childBorder = cat.surface2;
          };
          focusedInactive = {
            border = cat.surface1;
            background = cat.surface1;
            text = cat.subtext1;
            indicator = cat.teal;
            childBorder = cat.surface1;
          };
          unfocused = {
            border = cat.surface0;
            background = cat.surface0;
            text = cat.subtext0;
            indicator = cat.teal;
            childBorder = cat.surface0;
          };
          urgent = {
            border = cat.red;
            background = cat.maroon;
            text = cat.text;
            indicator = cat.red;
            childBorder = cat.red;
          };
        };

        menu = "${pkgs.rofi-wayland}/bin/rofi -show drun -modes drun,run";
        terminal = cfg.terminal.binPath;

        focus.mouseWarping = true;
        focus.wrapping = "yes";

        keybindings = let
          modifier = config.wayland.windowManager.sway.config.modifier;
          screenshotRegion = "jwm screenshot-region";
          # swaymsg -t get_tree | jq '.. | select(type == "object") | select (has("type") and (.type == "con" or .type == "floating_con")) | select(.visible) | "\(.rect.x),\(.rect.y) \(.rect.width)x\(.rect.height) \(.name)"' -r | slurp -r | grim -g - - | wl-copy
          volUp = "jwm vol 5";
          volDown = "jwm vol -5";
          volMute = "jwm vol toggle-mute";
          brightUp = "jwm bright 5";
          brightDown = "jwm bright -5";
          musPlay = "playerctl play-pause";
        in lib.mkOptionDefault {
          "${modifier}+tab" = "workspace next";
          "${modifier}+grave" = "workspace prev";
          "${modifier}+q" = "exec ptrayctl hide";
          "${modifier}+t" = "layout tabbed";
          "${modifier}+p" = "mode command";
          "${modifier}+i" = "mode app";

          "--locked XF86MonBrightnessUp" = "exec ${brightUp}";
          "--locked XF86MonBrightnessDown" = "exec ${brightDown}";

          "--locked XF86AudioRaiseVolume" = "exec ${volUp}";
          "--locked XF86AudioLowerVolume" = "exec ${volDown}";
          "--locked XF86AudioMute" = "exec ${volMute}";
          "--locked XF86AudioPlay" = "exec ${musPlay}";
          "--locked XF86AudioNext" = "exec playerctl next";
          "--locked XF86AudioPrev" = "exec playerctl previous";
          "--locked XF86AudioStop" = "exec playerctl stop";

          "${modifier}+shift+s" = "exec ${screenshotRegion}";

          "${modifier}+minus" = "floating toggle";
          "${modifier}+shift+equal" = "move to scratchpad";
          "${modifier}+equal" =
            "rofi -show scratchpad -modes 'scratchpad:~/.config/rofi/bin/scratchpad'";
        };

        input = {
          "type:keyboard" = {
            xkb_options = lib.strings.concatStringsSep "," [
              "altwin:swap_lalt_lwin"
              "altwin:menu_win"
            ];
          };
          "type:touchpad" = {
            dwt = "enabled";
            tap = "enabled";
            natural_scroll = "enabled";
            middle_emulation = "enabled";
            drag = "enabled";
          };
        };

        window.hideEdgeBorders = "smart";

        modes = lib.mkOptionDefault {
          app = let
            discord = "${ptray}/bin/ptrayctl toggle discord";
            keepass = "${ptray}/bin/ptrayctl toggle keepass";
            web = "jwm web-launcher";
          in {
            Escape = "mode default";
            Return = "mode default";

            d = "exec ${discord},, mode default";
            k = "exec ${keepass},, mode default";
            w = "exec ${web},, mode default";
          };

          command = let
            switch = "jwm switch-launcher";
            micMute = "jwm mic toggle-mute";
            lock = "jwm lock";
            typeEmoji = "rofi -show emoji -emoji-mode stdout | xargs wtype";
          in {
            Escape = "mode default";
            Return = "mode default";

            "e" = "exec ${typeEmoji},, mode default";
            m = "exec ${micMute},, mode default";
            s = "exec ${switch},, mode default";
            l = "exec ${lock},, mode default";
          };
        };

        window.commands = [{
          command = "floating enable,, sticky enable,, opacity 0.9";
          criteria = { title = "Picture-in-Picture"; };
        }];

        output = { "*" = { bg = lib.mkDefault "${cfg.wallpaper} fill"; }; };

        seat = {
          "*" = { xcursor_theme = "Catppuccin-Mocha-Light-Cursors 32"; };
        };

        startup = [
          #{ command = ''plhelper switch "$(plhelper who)"''; always = true; }
          #{ command = "dropwind"; }
          { command = "mako"; }
          {
            command = "playerctld";
          }
          # {
          #   command =
          #     "${pkgs.xdg-desktop-portal-wlr}/libexec/xdg-desktop-portal-wlr";
          #   always = true;
          # }

        ];
      };
      extraConfig = ''
        hide_edge_borders --i3 smart
        for_window [title=".*"] inhibit_idle fullscreen
      '';
      extraOptions = [ "--unsupported-gpu" ];
    };
  };
}
