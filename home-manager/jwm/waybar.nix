{ lib, pkgs, config, ... }:

with lib;

let
  ptray = config.jainawm.ptray.finalPackage;
  watchPtrayIcon = name: displayName:
    pkgs.writeShellScript "watchPtrayIcon-${name}" ''
      trap 'kill 0' EXIT
      ${ptray}/bin/ptrayctl watch ${name} | while read line; do
        echo '{"text":"update","tooltip":"${displayName}","class":"'"$line"'"}'
      done
    '';
in {
  options.jainawm.waybar = { enable = mkEnableOption "Enable waybar"; };

  config = {
    programs.waybar = {
      enable = true;
      style = builtins.readFile ./waybar.css;
      #package = pkgs.unstable.waybar;
      settings = [{
        layer = "top";
        position = "top";
        mode = "dock";
        name = "main";

        modules-left = [
          "sway/workspaces"
          "sway/mode"

          "group/musicplayer"
        ];
        modules-center = [ "sway/window" ];
        modules-right = [
          "group/tray"
          "group/volume"
          "group/battery"

          "clock"
        ];

        "group/musicplayer" = {
          orientation = "horizontal";
          modules = [ "custom/player-icon" "custom/mpris" ];
        };

        "group/tray" = {
          orientation = "horizontal";
          modules =
            [ "idle_inhibitor" "tray" "custom/discord" "custom/keepass" ];
        };
        "tray" = { };

        "group/volume" = {
          orientation = "horizontal";
          modules =
            [ "pulseaudio#output-icon" "pulseaudio#output" "pulseaudio#input" ];
        };

        "group/battery" = {
          orientation = "horizontal";
          modules = [ "battery" "upower" ];
        };

        "custom/player-icon" =
          let playerctl = "${pkgs.playerctl}/bin/playerctl --player strawberry";
          in {
            format = "{icon}";
            format-icons = "";
            exec = ''
              trap 'kill 0' EXIT
              (
                echo "";
                ${playerctl} status --follow --format \
                  '{"text":"music","class":"{{lc(status)}}","tooltip":"strawberry"}'
              ) | (
                while read line; do
                  if [ -n "$line" ]; then
                    echo "$line"
                  else
                    echo '{"text":"music","tooltip":"strawberry"}'
                  fi
                done
              );
            '';
            on-click = ''
              ${pkgs.sway}/bin/swaymsg '[app_id=org.strawberrymusicplayer.] focus' >/dev/null \
                || swaymsg exec strawberry
            '';
            return-type = "json";
          };

        "custom/mpris" =
          let playerctl = "${pkgs.playerctl}/bin/playerctl --player strawberry";
          in {
            format = "{icon} {}";
            format-icons = {
              playing = "";
              paused = "";
            };
            exec = let
              playerctlFormat = ''
                {"title":"{{title}}","artist":"{{artist}}","album":"{{album}}","position":"{{duration(position)}}","length":"{{duration(mpris:length)}}","status":"{{lc(status)}}"}'';
              jqFilter = ''
                def escape: . | gsub("&"; "&amp;") | gsub("<"; "&lt;") | gsub(">"; "&gt;");
                try (
                  . + {
                    mainText: ("\([.title,.artist,.album] | map(select(length > 0)) | join(" - "))") | escape,
                    progressText: ((select((.position | length) > 0 and (.length | length > 0)) | "[\(.position)/\(.length)]" | escape) // "")
                  } | . + {
                    progress: ((. | select(.progressText | length > 0) | " <small>\(.progressText)</small>") // "")
                  } | {
                    text: (.mainText + .progress),
                    alt: (.status)
                  } | { text, alt, tooltip: (.text), class: (.alt) }
                ) catch ""
              '';
              mainFormat =
                "{{title}} - {{artist}} - {{album}} <small>[{{duration(position)}}/{{duration(mpris:length)}}]</small>";
            in ''
              trap 'kill 0' EXIT
              ${playerctl} metadata --follow --format '${playerctlFormat}' | (
                # playerctl has a bug where it only shows that a stopped stream has stopped after it starts again. but it does spit out an empty line. so we do a lot of work special-casing those
                while read line; do
                  if [ -n "$line" ]; then echo "$line"; else echo '" "'; fi
                done
              ) | jq '${jqFilter}' --unbuffered --compact-output --raw-output
            '';
            max-length = 80;
            on-click = "${playerctl} play-pause";
            on-click-middle = "${playerctl} previous";
            on-click-right = "${playerctl} next";
            return-type = "json";
          };

        idle_inhibitor = {
          format = "{icon}";
          format-icons = {
            activated = "";
            deactivated = "";
          };
          tooltip-format-activated = "Idle inhibitor enabled";
          tooltip-format-deactivated = "Idle inhibitor disabled";
        };

        "custom/discord" = {
          format = "";
          exec = watchPtrayIcon "discord" "Discord";
          on-click = "${ptray}/bin/ptrayctl toggle discord";
          restart-interval = 5;
          return-type = "json";
        };

        "custom/keepass" = {
          format = "";
          exec = watchPtrayIcon "keepass" "KeePassXC (Password Manager)";
          # exec =
          #   "~/.config/waybar/scripts/watchstatus keepass KeePassXC 'KeePassXC (Password Manager)'";
          #return-type = "json";
          on-click = "${ptray}/bin/ptrayctl toggle keepass";
          on-click-middle = "killall keepassxc";
          restart-interval = 5;
          return-type = "json";
        };

        "pulseaudio#output-icon" = {
          format = "{icon}";
          format-muted = "󰸈";

          format-icons = {
            headphone = "";
            hands-free = "";
            headset = "";
            phone = "";
            portable = "";
            car = "";
            default = [ "󰕿" "󰖀" "󰕾" ];
          };
          on-click = "jwm vol toggle-mute";
          on-click-right = "dropwinctl toggle pavucontrol";
        };

        "pulseaudio#output" = {
          format = "{volume}%";
          on-click = "jwm vol toggle-mute";
          on-click-right = "dropwinctl toggle pavucontrol";
        };

        "pulseaudio#input" = {
          format = "{format_source}";
          format-source = "";
          format-source-muted = "";
          on-click = "jwm mic toggle-mute";
          on-click-right = "dropwinctl toggle pavucontrol";
        };

        battery = {
          bat = "BAT0";
          states = {
            warning = 30;
            critical = 15;
          };
          format = "{icon}";
          rotate = 90;
          format-charging = "";
          tooltip-format = "Battery: {capacity}%";
          format-icons = [ "" "" "" "" "" "" "" "" "" "" ];
        };

        upower = {
          format = "{percentage}";
          format-alt = "{time}";
          icon-size = 1;
        };

        clock = {
          tooltip-format = ''
            <big>{:%Y %B}</big>
            <tt><small>{calendar}</small></tt>'';
        };
      }];
    };
  };
}
