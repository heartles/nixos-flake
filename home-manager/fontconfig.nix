{ config, pkgs, lib, ... }:

with lib;
let
  cfg = config.fonts.fontconfig-extra;
in {
  options.fonts.fontconfig-extra = {
    enable = mkOption {
      type = types.bool;
      default = false;
      description = ''
        Whether to enable advanced fontconfig configuration.
        This will allow you to set default fonts for your applications.
      '';
    };

    defaultFonts = let
      mkFontOption = name: mkOption {
        type = types.nullOr hm.types.fontType;
        default = null;
        description = ''
          The default font to use when a ${name} font is requested.
        '';
      };
    in {
      sans-serif = mkFontOption "sans-serif";
      serif = mkFontOption "serif";
      monospace = mkFontOption "monospace";
    };
  };

  config = mkIf cfg.enable {
    home.packages = let
      optionalPackage = opt: optional (opt != null && opt.package != null) opt.package;
    in concatMap optionalPackage [
      cfg.defaultFonts.sans-serif
      cfg.defaultFonts.serif
      cfg.defaultFonts.monospace
    ];

    xdg.configFile."fontconfig/conf.d/80-hm-fonts-extra.conf".text = let
      mkConfig = family: font: mapNullable (notNullFont: ''
        <match target="pattern">
          <test name="family" qual="any">
            <string>${family}</string>
          </test>
          <edit binding="strong" mode="prepend" name="family">
            <string>${font.name}</string>
          </edit>
        </match>
      '') font;
      configs = concatStrings (filter (v: ! isNull v) (
        attrValues (mapAttrs mkConfig cfg.defaultFonts)
      ));
    in ''
      <?xml version="1.0"?>
      <!DOCTYPE fontconfig SYSTEM "urn:fontconfig:fonts.dtd">
      <fontconfig>
        ${configs}
      </fontconfig>
    '';
  };
}
