{ config, pkgs, lib, ... }:

# TODOs:
# - Add in configs for:
#   + Rofi
#   + mako
# - Setup better on-screen-keyboard support
# - rofi-emoji
# - Create settings bar or something
# - Figure out how to get info or widgets on lockscreen
# - Explore Hyprland

let
  fonts = {
    primary = {
      name = "Iosevka Comfy Duo";
      package = pkgs.iosevka-comfy.comfy-duo;
      size = 11;
    };
    serif = {
      name = "Iosevka Comfy Motion Duo";
      package = pkgs.iosevka-comfy.comfy-motion-duo;
      size = 11;
    };
    fixed = {
      name = "Iosevka Comfy Wide";
      package = pkgs.iosevka-comfy.comfy-wide;
      size = 11;
    };

    wmOverlay =
      lib.trivial.mergeAttrs fonts.primary { size = fonts.primary.size + 1; };
  };
  fronters = {
    jams = {
      name = "jams";
      displayName = "jams";
      accents = {
        primary = "Green";
        secondary = "Teal";
      };
      wallpaper = ./wallpapers/slimejaina_extinct_lesboba.png;
    };

    velma = {
      name = "velma";
      displayName = "Velma Jade";
      accents = {
        primary = "Peach";
        secondary = "Yellow";
      };
      wallpaper = ./wallpapers/velma_picrew_cropped.png;
    };

    alys = {
      name = "alys";
      displayName = "alys elrena";
      nicknames = [ "elrena" ];
      accents = {
        primary = "Blue";
        secondary = "Sapphire";
      };
      wallpaper = ./wallpapers/elrena-lauriam-cropped.jpg;
    };

    sarina = {
      name = "sarina";
      displayName = "Sarina";
      nicknames = [ "rachel" ];
      accents = {
        primary = "Pink";
        secondary = "Mauve";
      };
      wallpaper = ./wallpapers/sarina.png;
    };

    denise = {
      name = "denise";
      displayName = "denise_";
      nicknames = [ "0x7F454C46" "_denise" ];
      accents = {
        primary = "Lavender";
        secondary = "Blue";
      };
      wallpaper = ./wallpapers/denise_picrew_2.png;
    };
  };
  defaultFront = fronters.jams;
in {
  nixpkgs.config.allowUnfree = true;

  imports = [ ./jwm.nix ./fontconfig.nix ];

  jainawm = {
    enable = true;
    fonts = fonts;
    phelper = {
      profiles = fronters;
      defaultProfile = "jams";
      makeSpecializations = true;
    };
  };
  # specialization = builtins.mapAttrs (n: c: { configuration = {
  #   jainawm.phelper.activeProfile = n;
  #   home.activation.recordVersion = lib.hm.dag.entryAnywhere "";
  # }; }) fronters;

  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "jaina";
  home.homeDirectory = "/home/jaina";

  fonts.fontconfig-extra = {
    enable = true;
    defaultFonts = {
      sans-serif = fonts.primary;
      serif = fonts.serif;
      monospace = fonts.fixed;
    };
  };

  xdg.enable = true;
  xdg.cacheHome = builtins.toPath "${config.home.homeDirectory}/.cache";
  xdg.configHome = builtins.toPath "${config.home.homeDirectory}/.config";
  xdg.dataHome = builtins.toPath "${config.home.homeDirectory}/.local/share";
  xdg.stateHome = builtins.toPath "${config.home.homeDirectory}/.local/state";
  #xdg.mimeApps = {};
  xdg.userDirs = {
    enable = true;
    desktop = null;
    publicShare = null;
    templates = null;
    documents = "${config.home.homeDirectory}/documents";
    download = "${config.home.homeDirectory}/downloads";
    music = "${config.home.homeDirectory}/music";
    pictures = "${config.home.homeDirectory}/pictures";
    videos = "${config.home.homeDirectory}/videos";
  };

  xdg.desktopEntries = {
    webcord = {
      name = "WebCord";
      terminal = false;
      exec =
        "webcord --enable-features=UseOzonePlatform --ozone-platform=wayland --disable-gpu";
    };
  };

  # home.activation = {
  #   recordVersion = lib.mkDefault (lib.hm.dag.entryAfter ["writeBoundary"] ''
  #     rm -f "${config.xdg.stateHome}/hm-generation"
  #     ${pkgs.coreutils}/bin/ln -s "$(
  #       ${pkgs.home-manager}/bin/home-manager generations | \
  #         ${pkgs.coreutils}/bin/head -n1 | \
  #         ${pkgs.gawk}/bin/awk '{ print $7; }'
  #       )" \
  #       "${config.xdg.stateHome}/hm-generation"
  #   '');
  # };

  gtk = {
    enable = true;
    font = fonts.primary;
  };
  home.pointerCursor = {
    gtk.enable = true;
    x11.enable = true;
    name = "Catppuccin-Mocha-Light-Cursors";
    size = 32;
    package = pkgs.catppuccin-cursors.mochaLight;
  };

  home.enableNixpkgsReleaseCheck = true;

  programs.kitty = {
    enable = true;
    font = fonts.fixed;
    theme = "Catppuccin-Mocha";

    settings = {
      background_opacity = "0.85";
      show_hyperlink_targets = true;
      enable_audio_bell = false;
      visual_bell_duration = "0.05";
    };
  };

  home.packages = with pkgs; [
    neofetch
    less
    fish
    killall
    htop
    wget
    binutils
    coreutils-full
    zip
    unzip
    git

    #sway
    #rofi-wayland
    #waybar
    bc
    mako
    python3
    swaybg
    swayidle
    brightnessctl
    pamixer
    libnotify
    inotify-tools
    wl-clipboard
    pulseaudio
    pipewire
    sqlite
    grim
    slurp
    swaylock-effects
    xdg-user-dirs

    firefox

    firefox-devedition-bin

    qutebrowser
    #qutebrowser-qt6
    kakoune
    kitty
    keepassxc
    discord
    webcord

    pavucontrol
    evince

    # (steam.override {
    #   extraPkgs = pkgs:
    #     with pkgs; [
    #       xorg.libXcursor
    #       xorg.libXi
    #       xorg.libXinerama
    #       xorg.libXScrnSaver
    #       libpng
    #       libpulseaudio
    #       libvorbis
    #       stdenv.cc.cc.lib
    #       libkrb5
    #       keyutils

    #       # GW2
    #       libpng12
    #       gnutls
    #       freetype
    #       zlib
    #       #llvm
    #     ];
    # })
    steam
    gamemode
    lutris
    #wine
    #wine64
    wineWowPackages.stableFull
    SDL2
    vulkan-loader

    mpd

    mpv

    #rofi-emoji
    squeekboard
    wtype

    qbittorrent
    qbittorrent-nox
    nftables

    fabric-installer
    minecraft-server
    prismlauncher

    syncthing

    nixos-generators

    qemu_kvm
    OVMF.fd

    clementine

    gnumake
    gcc

    appimage-run

    shticker-book-unwritten

    jq

    iosevka-comfy.comfy-duo
    iosevka-comfy.comfy-wide-duo
    iosevka

    font-awesome
    (nerdfonts.override { fonts = [ "NerdFontsSymbolsOnly" ]; })

    pinta

    krita
    blender

    p7zip

    go
    #zig
    #unstable.zig_0_11

    llvm
    libclang

    luajitPackages.fennel
    fnlfmt

    lmms

    aspell

    libreoffice

    #yarn
    #postgresql
    sqlite
    #redis

    emacs

    #rofi-wayland

    colmena

    liferea
    evolution
    okular

    nixfmt

    stable.dolphin-emu

    glxinfo

    #any-nix-shell
    nix-your-shell

    gdb

    scdl
    ffmpeg
    unstable.yt-dlp
    stable.spotdl

    strawberry

    element-desktop

    #(eww.override { withWayland = true; })
    eww
    socat

    zscroll

    file

    heroic

    #nixgl.auto.nixVulkanNvidia
    #nixgl.auto.nixGLNvidiaBumblebee

    opusTools

    shotcut
    kdePackages.kdenlive
    glaxnimate
    mediainfo

    dig
    vlc
  ]; # end pkgs

  fonts.fontconfig.enable = true;

  programs.command-not-found.enable = true;

  home.sessionPath = [ "${config.home.homeDirectory}/.local/bin" ];
  home.sessionVariables = {
    VISUAL = "kak";
    EDITOR = "kak";
    BROWSER = "qutebrowser";
    PAGER = "less";
    TERMINAL = "kitty";

    SSH_AUTH_SOCK = "$XDG_RUNTIME_DIR/ssh-agent.socket";
  };

  programs.fish = {
    enable = true;
    shellAliases = {
      l = "ls -lah";
      psa = "ps aux";
      swaytree = "swaymsg -p -t get_tree | less";
      apcurl = "curl -H 'Accept: application/activity+json'";
      sway-game-output =
        "swaymsg 'output eDP-1 scale 1' && swaymsg 'output eDP-1 mode --custom 1920x1080' && swaymsg 'input * pointer_accel 0.35'";
      sway-normal-output =
        "swaymsg 'output eDP-1 scale 2' && swaymsg 'output eDP-1 mode 3000x2000' && swaymsg 'input * pointer_accel 0'";
      yt-mp3 =
        "yt-dlp --extract-audio --audio-quality 0 --embed-metadata --embed-thumbnail";
    };
    interactiveShellInit = ''
      neofetch
      if test -z "$DISPLAY" && test (tty) = "/dev/tty1"
        echo "Launching sway...."
        exec sh -c "sway | grep -v 'not resolve keysym' >$XDG_RUNTIME_DIR/sway.log 2>&1"
      end
    '';
    shellInit = ''
      #any-nix-shell fish --info-right | source
      if command -q nix-your-shell
        nix-your-shell fish | source
      end
    '';
  };

  programs.swaylock = {
    enable = true;
    package = pkgs.swaylock-effects;
    settings = {
      ignore-empty-password = true;
      show-failed-attempts = true;
      image = lib.mkDefault "${defaultFront.wallpaper}";
      effect-blur = "5x5";
      clock = true;
    };
  };

  services.swayidle = let
    mkFullscreenCheck = let
      jqFilter = ''
        ..
          | select (type == "object")
          | select (has("type") and (.type == "con" or .type == "floating_con"))
          | select (.visible and .fullscreen_mode != 0)
      '';
    in cmd:
    toString (pkgs.writeScript "fullscreen-check-wrapper" ''
      #!/bin/sh
      fullscreen_windows="$(${pkgs.sway}/bin/swaymsg -t get_tree | ${pkgs.jq}/bin/jq ${
        lib.strings.escapeShellArg jqFilter
      })"
      if [ -z "$fullscreen_windows" ]; then
        ${cmd}
      # else
        # Ignoring timeout command because there is a fullscreen window active
        # (such as a game or a movie)
      fi
    '');
    grace-period = 60;
    lock-timeout = 300;
    screen-timeout = 450;
    suspend-timeout = 600;
    lock-cmd = "${pkgs.swaylock-effects}/bin/swaylock --daemonize";
    set-screen = state: "${pkgs.sway}/bin/swaymsg 'output * dpms ${state}'";
  in {
    enable = true;
    events = [
      {
        event = "before-sleep";
        command = "${pkgs.systemd}/bin/loginctl lock-session";
      }
      {
        event = "lock";
        command = "${lock-cmd}";
      }
      {
        event = "after-resume";
        command = set-screen "on";
      }
    ];
    timeouts = [
      {
        timeout = lock-timeout;
        command = mkFullscreenCheck
          "${lock-cmd} --fade-in=3 --grace=${toString grace-period}";
      }
      {
        timeout = screen-timeout;
        command = set-screen "off";
        resumeCommand = set-screen "on";
      }
      {
        timeout = suspend-timeout;
        command = "${pkgs.systemd}/bin/systemctl suspend";
      }
    ];
  };

  services.syncthing.enable = true;

  programs.mangohud.enable = true;

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "22.11";
}
