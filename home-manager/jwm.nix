{ config, pkgs, lib, ... }:

with lib;
with builtins;
let cfg = config.jainawm;
in {
  options.jainawm = {
    enable = mkOption {
      type = types.bool;
      default = false;
    };

    terminal = {
      name = mkOption {
        type = types.str;
        default = "kitty";
      };
      package = mkOption {
        type = types.package;
        default = pkgs.kitty;
      };
      binPath = mkOption {
        type = types.str;
        readOnly = true;
      };
    };

    fonts = let fontOption = mkOption { type = lib.hm.types.fontType; };
    in {
      primary = fontOption;
      serif = fontOption;
      fixed = fontOption;
      wmOverlay = fontOption;
    };

    catppuccin = mkOption {
      #type = types.attrs;
      default = import ./catppuccin/mocha.nix;
      description = ''
        Set of colors to use for theme configuration.
      '';
    };

    accents = {
      primary = mkOption { type = types.str; };
      secondary = mkOption { type = types.str; };
    };

    wallpaper = mkOption { type = types.path; };

    startup = mkOption {
      type = types.listOf (type.submodule {
        options = {
          enable = mkEnableOption "Enable the given startup command";

          command = mkOption { type = types.str; };

          always = mkOption {
            type = types.bool;
            default = false;
            descrioption =
              "If true, the command will be ran when the window manager is reset";
          };
        };
      });
    };
  };

  imports = [
    ./jwm/sway.nix
    ./jwm/rofi.nix
    ./jwm/phelper.nix
    ./jwm/kanshi.nix
    ./jwm/waybar.nix
    ./jwm/ptray.nix
  ];

  config = let
    cat = cfg.catppuccin;
    #front = getAttr cfg.selectedFront cfg.fronters;
    fonts = cfg.fonts;
  in mkIf cfg.enable {
    jainawm.terminal.binPath =
      "${cfg.terminal.package}/bin/${cfg.terminal.name}";
    services.mako = {
      enable = true;
      anchor = "top-right";

      height = 100;
      width = 500;
      padding = "10";
      layer = "overlay";

      font = "${fonts.primary.name} ${toString (fonts.primary.size + 1)}";
      textColor = cat.text;
      backgroundColor = "${cat.surface0}cc";
      progressColor = "over ${cat.surface2}cc";

      borderColor =
        builtins.getAttr (lib.strings.toLower cfg.accents.primary) cat;
      borderRadius = 10;
      borderSize = 3;

      defaultTimeout = null;
      maxVisible = null;
      # groupBy =
      # iconPath =
      extraConfig = ''
        on-button-left=invoke-default-action
        on-touch=invoke-default-action
        on-button-right=dismiss

        [app-name="discord"]
        layer=overlay
        on-button-left=exec /usr/bin/env dropwinctl show discord && makoctl invoke -n "$id" && makoctl dismiss -n "$id"

        [app-name="JWM"]
        layer=overlay

        [app-name="JWM" category="audio" body~="1\([0-9][1-9]|[1-9][0-9]\)"]
        background-color=${cat.surface2}f8
        progress-color=over ${cat.red}cc
      '';
    };

    #jainawm.wallpaper = front.wallpaper;
    jainawm.rofi.accents =
      mapAttrs (n: c: builtins.getAttr (lib.strings.toLower c) cat) cfg.accents;
    jainawm.rofi.font = fonts.wmOverlay;
    jainawm.ptray = {
      enable = true;
      items = {
        discord = {
          match_classes = [ "WebCord" "discord" ];
          exec =
            "${pkgs.webcord}/bin/webcord --enable-features=UseOzonePlatform --ozone-platform=wayland --disable-gpu";
        };

        keepass = {
          match_classes = [ "org.keepassxc.KeePassXC" ];
          exec = "${pkgs.keepassxc}/bin/keepassxc";
        };
      };
      #   drops = let
      #     fx = [
      #       "opacity 0.97"
      #       "floating enable"
      #       "resize set 80ppt 80ppt"
      #       "move position 10ppt -10"
      #       "sticky enable"
      #     ];
      #   in {
      #     discord = {
      #       match = [ "WebCord" "discord" ];
      #       start = "${pkgs.discord}/bin/discord";
      #       customization = fx;
      #     };
      #     keepass = {
      #       match = [ "org.keepassxc.KeePassXC" ];
      #       start = "${pkgs.keepassxc}/bin/keepassxc";
      #       customization = fx;
      #     };
      #     matrix = {
      #       match = [ "Element" ];
      #       start = "${element-desktop}/bin/element-desktop";
      #       customization = fx;
      #     };
      #   };
    };
    #jainawm.rofi.accent = "#ff0000";
    #jainawm.rofi.enable = true;
    #programs.waybar.style = builtins.readFile ./waybar/style.css;

    jainawm.kanshi.enable = true;
    jainawm.sway.enable = true;
    # TODO
    xdg.configFile."eww/theme_cat.scss".text = concatStringsSep "\n"
      (mapAttrsToList (name: value: "$cat-${name}: ${value};") cfg.catppuccin);
    xdg.configFile."eww/theme_jwm.scss".text = ''
      @import "theme_cat"
      $jwm-accent: $cat-${lib.strings.toLower cfg.accents.primary};
      $jwm-accent2: $cat-${lib.strings.toLower cfg.accents.secondary};
      $jwm-accent-contrast: $cat-crust;
    '';

    #jainawm.sway.useFx = true;

    home.packages = with pkgs; [ playerctl ];

    qt = {
      #enable = true;
      enable = false;
      platformTheme.name = "gtk";
    };

    gtk.enable = true;
    gtk.theme = {
      name = "Catppuccin-Mocha-Standard-${cfg.accents.primary}-Dark";
      package = pkgs.catppuccin-gtk.override {
        accents = [ (lib.strings.toLower cfg.accents.primary) ];
        variant = "mocha";
      };
    };
    programs.swaylock.settings.image = "${cfg.wallpaper}";
    #wayland.windowManager.sway.config.output."*".bg = "${front.wallpaper} fill";

    # programs.rofi.theme = import ./rofi-theme.nix {
    #   cat = cat;
    #   front = front;
    #   fonts = cfg.fonts;
    #   lib = lib;
    #   config = config;
    # };
  };
}
