{ pkgs, ... }:

{
  networking.firewall.allowedUDPPorts = [ 51820 ];

  networking.wireguard.interfaces.wg0 = {
    ips = [ "10.100.0.2/24" ];
    listenPort = 51820;

    privateKeyFile = "/home/jaina/wg-keys/private";

    peers = [
      {
        publicKey = "8g2nDmUFmTE4y+0zvpB6tD5ktoULHpWQw+bud5yA6GA="
        allowedIPs = [ "192.168.1.1/16" ];

        endpoint = "";
      }
    ];
  };
}
